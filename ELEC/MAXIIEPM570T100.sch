EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 36 35
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Oscillator:SG-7050CAN X3501
U 1 1 5F263FE9
P 5450 6400
F 0 "X3501" H 5794 6446 50  0000 L CNN
F 1 "SG-7050CAN" H 5794 6355 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_SeikoEpson_SG8002CA-4Pin_7.0x5.0mm" H 6150 6050 50  0001 C CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?dl=brief_SG7050CAN&lang=en" H 5350 6400 50  0001 C CNN
	1    5450 6400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3511
U 1 1 5F263466
P 5800 5900
F 0 "C3511" V 6052 5900 50  0000 C CNN
F 1 "100n" V 5961 5900 50  0000 C CNN
F 2 "" H 5838 5750 50  0001 C CNN
F 3 "~" H 5800 5900 50  0001 C CNN
	1    5800 5900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3501
U 1 1 5F264050
P 4950 6200
F 0 "R3501" H 5020 6246 50  0000 L CNN
F 1 "0" H 5020 6155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4880 6200 50  0001 C CNN
F 3 "~" H 4950 6200 50  0001 C CNN
	1    4950 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 6400 6350 6400
Wire Wire Line
	5950 5900 6150 5900
Wire Wire Line
	4950 6350 4950 6400
Wire Wire Line
	4950 6400 5150 6400
Wire Wire Line
	4950 6050 4950 5900
Wire Wire Line
	4950 5900 5450 5900
Connection ~ 5450 5900
Wire Wire Line
	5450 5900 5450 5750
Wire Wire Line
	5450 5900 5450 6100
Wire Wire Line
	5450 5900 5650 5900
Wire Wire Line
	5450 6700 5450 6900
Wire Wire Line
	1450 3250 1650 3250
Wire Wire Line
	1450 3150 1650 3150
Wire Wire Line
	2150 1400 1950 1400
$Comp
L Device:C_Small C3501
U 1 1 5F29F750
P 2150 1200
F 0 "C3501" H 2242 1246 50  0000 L CNN
F 1 "100n" H 2242 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2150 1200 50  0001 C CNN
F 3 "~" H 2150 1200 50  0001 C CNN
	1    2150 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3503
U 1 1 5F2C9091
P 2550 1200
F 0 "C3503" H 2642 1246 50  0000 L CNN
F 1 "100n" H 2642 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2550 1200 50  0001 C CNN
F 3 "~" H 2550 1200 50  0001 C CNN
	1    2550 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3505
U 1 1 5F2CA8F2
P 2950 1200
F 0 "C3505" H 3042 1246 50  0000 L CNN
F 1 "100n" H 3042 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2950 1200 50  0001 C CNN
F 3 "~" H 2950 1200 50  0001 C CNN
	1    2950 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3507
U 1 1 5F2CAEC3
P 3350 1200
F 0 "C3507" H 3442 1246 50  0000 L CNN
F 1 "100n" H 3442 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 1200 50  0001 C CNN
F 3 "~" H 3350 1200 50  0001 C CNN
	1    3350 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3502
U 1 1 5F2CD636
P 2150 1600
F 0 "C3502" H 2242 1646 50  0000 L CNN
F 1 "100n" H 2242 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2150 1600 50  0001 C CNN
F 3 "~" H 2150 1600 50  0001 C CNN
	1    2150 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3504
U 1 1 5F2CD63C
P 2550 1600
F 0 "C3504" H 2642 1646 50  0000 L CNN
F 1 "100n" H 2642 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2550 1600 50  0001 C CNN
F 3 "~" H 2550 1600 50  0001 C CNN
	1    2550 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3506
U 1 1 5F2CD642
P 2950 1600
F 0 "C3506" H 3042 1646 50  0000 L CNN
F 1 "100n" H 3042 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2950 1600 50  0001 C CNN
F 3 "~" H 2950 1600 50  0001 C CNN
	1    2950 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3508
U 1 1 5F2CD648
P 3350 1600
F 0 "C3508" H 3442 1646 50  0000 L CNN
F 1 "100n" H 3442 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3350 1600 50  0001 C CNN
F 3 "~" H 3350 1600 50  0001 C CNN
	1    3350 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 1100 2150 1100
Wire Wire Line
	2150 1100 2550 1100
Connection ~ 2150 1100
Wire Wire Line
	2550 1100 2950 1100
Connection ~ 2550 1100
Wire Wire Line
	2950 1100 3350 1100
Connection ~ 2950 1100
Wire Wire Line
	2150 1400 2150 1300
Wire Wire Line
	2150 1400 2150 1500
Connection ~ 2150 1400
Wire Wire Line
	3350 1400 3350 1300
Wire Wire Line
	3350 1500 3350 1400
Connection ~ 3350 1400
Wire Wire Line
	2950 1500 2950 1400
Connection ~ 2950 1400
Wire Wire Line
	2950 1400 3350 1400
Wire Wire Line
	2950 1300 2950 1400
Wire Wire Line
	2550 1300 2550 1400
Connection ~ 2550 1400
Wire Wire Line
	2550 1400 2950 1400
Wire Wire Line
	2550 1400 2550 1500
Wire Wire Line
	2150 1400 2550 1400
Wire Wire Line
	1850 1700 2150 1700
Wire Wire Line
	2150 1700 2550 1700
Connection ~ 2150 1700
Wire Wire Line
	2550 1700 2950 1700
Connection ~ 2550 1700
Wire Wire Line
	2950 1700 3350 1700
Connection ~ 2950 1700
Wire Wire Line
	2450 1950 2450 2150
Wire Wire Line
	2000 1950 2250 1950
Wire Wire Line
	2550 1950 2450 1950
Connection ~ 2450 1950
Wire Wire Line
	2550 1950 2550 2150
Wire Wire Line
	2650 2150 2650 1950
Wire Wire Line
	2650 1950 2550 1950
Connection ~ 2550 1950
Wire Wire Line
	2750 2150 2750 1950
Wire Wire Line
	2750 1950 2650 1950
Connection ~ 2650 1950
Wire Wire Line
	2850 2150 2850 1950
Wire Wire Line
	2850 1950 2750 1950
Connection ~ 2750 1950
Wire Wire Line
	2950 2150 2950 1950
Wire Wire Line
	2950 1950 2850 1950
Connection ~ 2850 1950
Wire Wire Line
	3050 2150 3050 1950
Wire Wire Line
	3050 1950 2950 1950
Connection ~ 2950 1950
Wire Wire Line
	3150 2150 3150 1950
Wire Wire Line
	3150 1950 3050 1950
Connection ~ 3050 1950
$Comp
L Connector_Generic:Conn_02x05_Odd_Even JTAG3501
U 1 1 5F2F8394
P 5300 3950
F 0 "JTAG3501" H 5350 4367 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 5350 4276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 5300 3950 50  0001 C CNN
F 3 "~" H 5300 3950 50  0001 C CNN
	1    5300 3950
	1    0    0    -1  
$EndComp
Wire Notes Line
	6750 5300 4600 5300
Wire Notes Line
	4600 5300 4600 7300
Wire Notes Line
	4600 7300 6750 7300
Wire Notes Line
	6750 7300 6750 5300
Text Notes 4650 7250 0    50   ~ 0
50MHz REF_OSC
Wire Wire Line
	5900 3850 5600 3850
Wire Wire Line
	5600 3750 5950 3750
Wire Wire Line
	5950 4150 5600 4150
Wire Wire Line
	5050 4150 5100 4150
Wire Wire Line
	5050 3950 5100 3950
Wire Wire Line
	5050 3850 5100 3850
Wire Wire Line
	5100 3750 5050 3750
NoConn ~ 5600 3950
NoConn ~ 5600 4050
NoConn ~ 5100 4050
Wire Wire Line
	1500 6050 1650 6050
Wire Wire Line
	1500 6150 1650 6150
Wire Wire Line
	1500 6250 1650 6250
Wire Wire Line
	1500 6350 1650 6350
Wire Wire Line
	2450 6750 2450 6900
Wire Wire Line
	2050 6900 2250 6900
Wire Wire Line
	2550 6900 2450 6900
Connection ~ 2450 6900
Wire Wire Line
	2550 6750 2550 6900
Wire Wire Line
	2650 6750 2650 6900
Wire Wire Line
	2650 6900 2550 6900
Connection ~ 2550 6900
Wire Wire Line
	2750 6750 2750 6900
Wire Wire Line
	2750 6900 2650 6900
Connection ~ 2650 6900
Wire Wire Line
	2850 6750 2850 6900
Wire Wire Line
	2850 6900 2750 6900
Connection ~ 2750 6900
Wire Wire Line
	2950 6750 2950 6900
Wire Wire Line
	2950 6900 2850 6900
Connection ~ 2850 6900
Wire Wire Line
	3050 6900 2950 6900
Connection ~ 2950 6900
Wire Wire Line
	3050 6750 3050 6900
Wire Wire Line
	3150 6900 3050 6900
Connection ~ 3050 6900
Wire Wire Line
	3150 6750 3150 6900
$Comp
L Device:R R3505
U 1 1 5F358E29
P 5350 4800
F 0 "R3505" V 5400 4550 50  0000 C CNN
F 1 "10k" V 5400 4300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5280 4800 50  0001 C CNN
F 3 "~" H 5350 4800 50  0001 C CNN
	1    5350 4800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3504
U 1 1 5F35E4A4
P 5350 4600
F 0 "R3504" V 5400 4350 50  0000 C CNN
F 1 "10k" V 5400 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5280 4600 50  0001 C CNN
F 3 "~" H 5350 4600 50  0001 C CNN
	1    5350 4600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3503
U 1 1 5F360675
P 5350 4500
F 0 "R3503" V 5400 4250 50  0000 C CNN
F 1 "10k" V 5400 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5280 4500 50  0001 C CNN
F 3 "~" H 5350 4500 50  0001 C CNN
	1    5350 4500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3502
U 1 1 5F36757C
P 5350 4400
F 0 "R3502" V 5400 4150 50  0000 C CNN
F 1 "10k" V 5400 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5280 4400 50  0001 C CNN
F 3 "~" H 5350 4400 50  0001 C CNN
	1    5350 4400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 4400 5950 4400
Wire Wire Line
	5050 4400 5200 4400
Wire Wire Line
	5050 4500 5200 4500
Wire Wire Line
	5050 4600 5200 4600
Wire Wire Line
	5500 4500 5900 4500
Wire Wire Line
	5500 4600 5900 4600
Wire Wire Line
	5500 4800 5900 4800
Wire Wire Line
	5050 4800 5200 4800
Wire Notes Line
	6350 3400 4600 3400
Wire Notes Line
	4600 3400 4600 5050
Wire Notes Line
	4600 5050 6350 5050
Wire Notes Line
	6350 5050 6350 3400
Text Notes 4650 5000 0    50   ~ 0
JTAG_USB_BLASTER
Wire Notes Line
	4400 900  900  900 
Wire Notes Line
	900  900  900  7350
Wire Notes Line
	900  7350 4400 7350
Wire Notes Line
	4400 7350 4400 900 
Text Notes 950  7300 0    50   ~ 0
CPLD_MAXIIEPMxx
Text Label 1850 1100 2    50   ~ 0
+3.3
Text Label 1850 1700 2    50   ~ 0
+3.3
Text Label 2000 1950 2    50   ~ 0
+3.3
Text Label 5450 5750 1    50   ~ 0
+3.3
Text Label 5900 4500 0    50   ~ 0
+3.3
Text Label 5900 4600 0    50   ~ 0
+3.3
Text Label 5900 4800 0    50   ~ 0
+3.3
Text Label 5900 3850 0    50   ~ 0
+3.3
Text Label 2050 6900 2    50   ~ 0
GND
Text Label 5450 6900 3    50   ~ 0
GND
Text Label 6150 5900 0    50   ~ 0
GND
Text Label 5950 3750 0    50   ~ 0
GND
Text Label 5950 4150 0    50   ~ 0
GND
Text Label 5950 4400 0    50   ~ 0
GND
Text Label 1950 1400 2    50   ~ 0
GND
Text Label 6350 6400 0    50   ~ 0
CLK0
Text Label 1450 3150 2    50   ~ 0
CLK0
Text Label 1450 3250 2    50   ~ 0
CLK1
Text Label 3950 3350 0    50   ~ 0
CLK2
Text Label 3950 3450 0    50   ~ 0
CLK3
Text Label 1500 6050 2    50   ~ 0
TMS
Text Label 1500 6150 2    50   ~ 0
TDI
Text Label 1500 6250 2    50   ~ 0
TCK
Text Label 1500 6350 2    50   ~ 0
TDO
Text Label 5050 4600 2    50   ~ 0
TMS
Text Label 5050 4150 2    50   ~ 0
TDI
Text Label 5050 3750 2    50   ~ 0
TCK
Text Label 5050 3850 2    50   ~ 0
TDO
Text Label 5050 3950 2    50   ~ 0
TMS
Text Label 5050 4500 2    50   ~ 0
TDO
Text Label 5050 4400 2    50   ~ 0
TCK
Text Label 5050 4800 2    50   ~ 0
TDI
Text HLabel 4950 1050 0    50   Input ~ 0
+3.3
Text HLabel 4950 1150 0    50   Input ~ 0
GND
Text HLabel 4950 1300 0    50   Input ~ 0
CLK0
Text HLabel 4950 1400 0    50   Input ~ 0
CLK1
Text HLabel 4950 1500 0    50   Input ~ 0
CLK2
Text HLabel 4950 1600 0    50   Input ~ 0
CLK3
Text HLabel 4950 1750 0    50   Input ~ 0
TCK
Text HLabel 4950 1850 0    50   Input ~ 0
TDO
Text HLabel 4950 1950 0    50   Input ~ 0
TMS
Text HLabel 4950 2050 0    50   Input ~ 0
TDI
Wire Notes Line
	4600 900  4600 3200
Wire Notes Line
	4600 3200 9700 3200
Wire Notes Line
	9700 3200 9700 900 
Wire Notes Line
	9700 900  4600 900 
Text Label 5150 1050 0    50   ~ 0
+3.3
Text Label 5150 1150 0    50   ~ 0
GND
Text Label 5150 1300 0    50   ~ 0
CLK0
Text Label 5150 1400 0    50   ~ 0
CLK1
Text Label 5150 1500 0    50   ~ 0
CLK2
Text Label 5150 1600 0    50   ~ 0
CLK3
Text Label 5150 1750 0    50   ~ 0
TCK
Text Label 5150 1850 0    50   ~ 0
TDO
Text Label 5150 1950 0    50   ~ 0
TMS
Text Label 5150 2050 0    50   ~ 0
TDI
Wire Wire Line
	4950 1050 5150 1050
Wire Wire Line
	4950 1150 5150 1150
Wire Wire Line
	4950 1300 5150 1300
Wire Wire Line
	4950 1400 5150 1400
Wire Wire Line
	4950 1500 5150 1500
Wire Wire Line
	4950 1600 5150 1600
Wire Wire Line
	4950 1750 5150 1750
Wire Wire Line
	4950 1850 5150 1850
Wire Wire Line
	4950 1950 5150 1950
Wire Wire Line
	4950 2050 5150 2050
Wire Wire Line
	1650 2450 1550 2450
Text Label 1550 2450 2    50   ~ 0
IO2
Wire Wire Line
	1650 2950 1550 2950
Text Label 1550 2950 2    50   ~ 0
IO7
Wire Wire Line
	1650 3750 1550 3750
Text Label 1550 3750 2    50   ~ 0
IO19
Wire Wire Line
	1650 4550 1550 4550
Text Label 1550 4550 2    50   ~ 0
IO33_ECLK
Wire Wire Line
	1650 4250 1550 4250
Text Label 1550 4250 2    50   ~ 0
IO28_RX
Wire Wire Line
	1650 4950 1550 4950
Text Label 1550 4950 2    50   ~ 0
IO38
Wire Wire Line
	1650 5550 1550 5550
Text Label 1550 5550 2    50   ~ 0
IO47
Wire Wire Line
	1650 5850 1550 5850
Wire Wire Line
	5850 3050 6050 3050
Text Label 6050 3050 0    50   ~ 0
IO51
Text HLabel 5850 3050 0    50   Input ~ 0
IO51
Wire Wire Line
	5850 2350 6050 2350
Text Label 6050 2350 0    50   ~ 0
IO42
Text HLabel 5850 2350 0    50   Input ~ 0
IO42
Wire Wire Line
	5850 2450 6050 2450
Text Label 6050 2450 0    50   ~ 0
IO43
Text HLabel 5850 2450 0    50   Input ~ 0
IO43
Wire Wire Line
	5850 2550 6050 2550
Text Label 6050 2550 0    50   ~ 0
IO44
Text HLabel 5850 2550 0    50   Input ~ 0
IO44
Wire Wire Line
	5850 2650 6050 2650
Text Label 6050 2650 0    50   ~ 0
IO47
Text HLabel 5850 2650 0    50   Input ~ 0
IO47
Wire Wire Line
	5850 2750 6050 2750
Text Label 6050 2750 0    50   ~ 0
IO48
Text HLabel 5850 2750 0    50   Input ~ 0
IO48
Wire Wire Line
	5850 2850 6050 2850
Text Label 6050 2850 0    50   ~ 0
IO49
Text HLabel 5850 2850 0    50   Input ~ 0
IO49
Wire Wire Line
	5850 2950 6050 2950
Text Label 6050 2950 0    50   ~ 0
IO50
Text HLabel 5850 2950 0    50   Input ~ 0
IO50
Text Label 1550 5850 2    50   ~ 0
IO50
Wire Wire Line
	5850 2050 6050 2050
Wire Wire Line
	5850 1950 6050 1950
Text Label 3950 2450 0    50   ~ 0
IO1
Text Label 6050 2050 0    50   ~ 0
IO33_ECLK
Text Label 6050 1950 0    50   ~ 0
IO28_RX
Text HLabel 5850 2050 0    50   Input ~ 0
IO33_ECLK
Text HLabel 5850 1950 0    50   Input ~ 0
IO28_RX
Text Label 3950 6350 0    50   ~ 0
IO100
Text Label 3950 6150 0    50   ~ 0
IO98
Text Label 3950 5950 0    50   ~ 0
IO96
Text Label 3950 5750 0    50   ~ 0
IO92
Text Label 3950 5550 0    50   ~ 0
IO89
Text Label 3950 5350 0    50   ~ 0
IO86
Wire Wire Line
	4950 3050 5150 3050
Text Label 5150 3050 0    50   ~ 0
IO100
Wire Wire Line
	4950 2950 5150 2950
Text Label 5150 2950 0    50   ~ 0
IO98
Wire Wire Line
	4950 2850 5150 2850
Text Label 5150 2850 0    50   ~ 0
IO96
Wire Wire Line
	4950 2750 5150 2750
Text Label 5150 2750 0    50   ~ 0
IO92
Wire Wire Line
	4950 2650 5150 2650
Text Label 5150 2650 0    50   ~ 0
IO89
Wire Wire Line
	4950 2550 5150 2550
Text Label 5150 2550 0    50   ~ 0
IO86
Wire Wire Line
	4950 2450 5150 2450
Text Label 5150 2450 0    50   ~ 0
IO84
Wire Wire Line
	4950 2350 5150 2350
Text Label 5150 2350 0    50   ~ 0
IO82
Text HLabel 4950 2350 0    50   Input ~ 0
IO82
Text HLabel 4950 2450 0    50   Input ~ 0
IO84
Text HLabel 4950 2550 0    50   Input ~ 0
IO86
Text HLabel 4950 2650 0    50   Input ~ 0
IO89
Text HLabel 4950 2750 0    50   Input ~ 0
IO92
Text HLabel 4950 2850 0    50   Input ~ 0
IO96
Text HLabel 4950 2950 0    50   Input ~ 0
IO98
Text HLabel 4950 3050 0    50   Input ~ 0
IO100
Text Label 1550 4350 2    50   ~ 0
IO29_TX
Wire Wire Line
	1550 4350 1650 4350
Wire Wire Line
	5850 1850 6050 1850
Text Label 6050 1850 0    50   ~ 0
IO29_TX
Text HLabel 5850 1850 0    50   Input ~ 0
IO29_TX
Wire Wire Line
	1650 4750 1550 4750
Wire Wire Line
	1650 4850 1550 4850
Wire Wire Line
	1650 5150 1550 5150
Text Label 1550 5150 2    50   ~ 0
IO41
Wire Wire Line
	1650 5250 1550 5250
Wire Wire Line
	1650 5350 1550 5350
Text Label 1550 5350 2    50   ~ 0
IO43
Text Label 1550 5250 2    50   ~ 0
IO42
Text Label 3950 2550 0    50   ~ 0
IO52
Text Label 3950 2650 0    50   ~ 0
IO53
Text Label 3950 2750 0    50   ~ 0
IO54
Text Label 3950 2850 0    50   ~ 0
IO55
Text Label 3950 2950 0    50   ~ 0
IO56
Text Label 3950 3050 0    50   ~ 0
IO57
Text Label 3950 3150 0    50   ~ 0
IO58
Text Label 3950 3650 0    50   ~ 0
IO67
Text Label 3950 3750 0    50   ~ 0
IO68
Text Label 3950 3850 0    50   ~ 0
IO69
Text Label 3950 3950 0    50   ~ 0
IO70
Text Label 3950 4050 0    50   ~ 0
IO71
Text Label 3950 4150 0    50   ~ 0
IO72
Text Label 3950 4250 0    50   ~ 0
IO73
Text Label 3950 4350 0    50   ~ 0
IO74
Text Label 9400 1050 0    50   ~ 0
IO1
Wire Wire Line
	9300 1050 9400 1050
Text Label 9400 1150 0    50   ~ 0
IO52
Wire Wire Line
	9300 1150 9400 1150
Text Label 9400 1250 0    50   ~ 0
IO53
Wire Wire Line
	9300 1250 9400 1250
Text Label 9400 1350 0    50   ~ 0
IO54
Wire Wire Line
	9300 1350 9400 1350
Text Label 9400 1450 0    50   ~ 0
IO55
Wire Wire Line
	9300 1450 9400 1450
Text Label 9400 1550 0    50   ~ 0
IO56
Wire Wire Line
	9300 1550 9400 1550
Text Label 9400 1650 0    50   ~ 0
IO57
Wire Wire Line
	9300 1650 9400 1650
Text Label 9400 1750 0    50   ~ 0
IO58
Wire Wire Line
	9300 1750 9400 1750
Text Label 9400 1950 0    50   ~ 0
IO67
Wire Wire Line
	9300 1950 9400 1950
Text Label 9400 2050 0    50   ~ 0
IO68
Wire Wire Line
	9300 2050 9400 2050
Text Label 9400 2150 0    50   ~ 0
IO69
Wire Wire Line
	9300 2150 9400 2150
Text Label 9400 2250 0    50   ~ 0
IO70
Wire Wire Line
	9300 2250 9400 2250
Text Label 9400 2350 0    50   ~ 0
IO71
Wire Wire Line
	9300 2350 9400 2350
Text Label 9400 2450 0    50   ~ 0
IO72
Wire Wire Line
	9300 2450 9400 2450
Text Label 9400 2550 0    50   ~ 0
IO73
Wire Wire Line
	9300 2550 9400 2550
Text Label 9400 2650 0    50   ~ 0
IO74
Wire Wire Line
	9300 2650 9400 2650
Wire Wire Line
	1650 2550 1550 2550
Text Label 1550 2550 2    50   ~ 0
IO3
Wire Wire Line
	1650 2650 1550 2650
Text Label 1550 2650 2    50   ~ 0
IO4
Wire Wire Line
	1650 2750 1550 2750
Text Label 1550 2750 2    50   ~ 0
IO5
Wire Wire Line
	1650 2850 1550 2850
Text Label 1550 2850 2    50   ~ 0
IO6
Wire Wire Line
	1650 3550 1550 3550
Text Label 1550 3550 2    50   ~ 0
IO17
Wire Wire Line
	1650 3450 1550 3450
Text Label 1550 3450 2    50   ~ 0
IO16
Wire Wire Line
	1650 3650 1550 3650
Text Label 1550 3650 2    50   ~ 0
IO18
Wire Wire Line
	1650 4150 1550 4150
Text Label 1550 4150 2    50   ~ 0
IO27
Wire Wire Line
	1650 3950 1550 3950
Text Label 1550 3950 2    50   ~ 0
IO21
Wire Wire Line
	1650 3850 1550 3850
Text Label 1550 3850 2    50   ~ 0
IO20
Wire Wire Line
	1650 4050 1550 4050
Text Label 1550 4050 2    50   ~ 0
IO26
Wire Wire Line
	1650 5450 1550 5450
Text Label 1550 5450 2    50   ~ 0
IO44
Wire Wire Line
	1650 5750 1550 5750
Text Label 1550 5750 2    50   ~ 0
IO49
Wire Wire Line
	1650 5650 1550 5650
Text Label 1550 5650 2    50   ~ 0
IO48
Wire Wire Line
	7450 1050 7550 1050
Text Label 7550 1050 0    50   ~ 0
IO2
Wire Wire Line
	7450 1150 7550 1150
Text Label 7550 1150 0    50   ~ 0
IO3
Wire Wire Line
	7450 1350 7550 1350
Text Label 3950 4550 0    50   ~ 0
IO76
Text Label 3950 4650 0    50   ~ 0
IO77
Text Label 3950 4750 0    50   ~ 0
IO78
Text Label 3950 4450 0    50   ~ 0
IO75
Wire Wire Line
	7450 1650 7550 1650
Text Label 7550 1650 0    50   ~ 0
IO7
Wire Wire Line
	7450 1550 7550 1550
Text Label 7550 1550 0    50   ~ 0
IO6
Wire Wire Line
	7450 1850 7550 1850
Text Label 7550 1850 0    50   ~ 0
IO17
Wire Wire Line
	7450 1750 7550 1750
Text Label 7550 1750 0    50   ~ 0
IO16
Wire Wire Line
	1650 5050 1550 5050
Text Label 1550 5050 2    50   ~ 0
IO40
Wire Wire Line
	7450 2150 7550 2150
Text Label 7550 2150 0    50   ~ 0
IO19
Wire Wire Line
	7450 2050 7550 2050
Text Label 7550 2050 0    50   ~ 0
IO18
Wire Wire Line
	7450 2350 7550 2350
Text Label 7550 2350 0    50   ~ 0
IO21
Wire Wire Line
	7450 2250 7550 2250
Text Label 7550 2250 0    50   ~ 0
IO20
Wire Wire Line
	8350 1150 8450 1150
Text Label 8450 1150 0    50   ~ 0
IO27
Wire Wire Line
	8350 1050 8450 1050
Text Label 8450 1050 0    50   ~ 0
IO26
Wire Wire Line
	8350 1250 8450 1250
Text Label 8450 1250 0    50   ~ 0
IO34
Wire Wire Line
	8350 1350 8450 1350
Text Label 8450 1350 0    50   ~ 0
IO35
Wire Wire Line
	8350 1650 8450 1650
Text Label 8450 1650 0    50   ~ 0
IO38
Wire Wire Line
	8350 1750 8450 1750
Text Label 8450 1750 0    50   ~ 0
IO40
Wire Wire Line
	8350 1550 8450 1550
Text Label 8450 1550 0    50   ~ 0
IO36
Wire Wire Line
	8350 1850 8450 1850
Text Label 8450 1850 0    50   ~ 0
IO41
Text Label 8450 2050 0    50   ~ 0
IO75
Wire Wire Line
	8350 2050 8450 2050
Text Label 8450 2150 0    50   ~ 0
IO76
Wire Wire Line
	8350 2150 8450 2150
Text Label 8450 2250 0    50   ~ 0
IO77
Wire Wire Line
	8350 2250 8450 2250
Text Label 8450 2350 0    50   ~ 0
IO78
Wire Wire Line
	8350 2350 8450 2350
Text Label 7550 1350 0    50   ~ 0
IO5
Text Label 7550 1250 0    50   ~ 0
IO4
Wire Wire Line
	7450 1250 7550 1250
Text HLabel 9300 1050 0    50   Input ~ 0
IO1
Text HLabel 9300 1150 0    50   Input ~ 0
IO52
Text HLabel 9300 1250 0    50   Input ~ 0
IO53
Text HLabel 9300 1350 0    50   Input ~ 0
IO54
Text HLabel 9300 1450 0    50   Input ~ 0
IO55
Text HLabel 9300 1550 0    50   Input ~ 0
IO56
Text HLabel 9300 1650 0    50   Input ~ 0
IO57
Text HLabel 9300 1750 0    50   Input ~ 0
IO58
Text HLabel 9300 1950 0    50   Input ~ 0
IO67
Text HLabel 9300 2050 0    50   Input ~ 0
IO68
Text HLabel 9300 2150 0    50   Input ~ 0
IO69
Text HLabel 9300 2250 0    50   Input ~ 0
IO70
Text HLabel 9300 2350 0    50   Input ~ 0
IO71
Text HLabel 9300 2450 0    50   Input ~ 0
IO72
Text HLabel 9300 2550 0    50   Input ~ 0
IO73
Text HLabel 9300 2650 0    50   Input ~ 0
IO74
Text HLabel 8350 1050 0    50   Input ~ 0
IO26
Text HLabel 8350 1150 0    50   Input ~ 0
IO27
Text HLabel 8350 1250 0    50   Input ~ 0
IO34
Text HLabel 8350 1350 0    50   Input ~ 0
IO35
Text HLabel 8350 1550 0    50   Input ~ 0
IO36
Text HLabel 8350 1650 0    50   Input ~ 0
IO38
Text HLabel 8350 1750 0    50   Input ~ 0
IO40
Text HLabel 8350 1850 0    50   Input ~ 0
IO41
Text HLabel 8350 2050 0    50   Input ~ 0
IO75
Text HLabel 8350 2150 0    50   Input ~ 0
IO76
Text HLabel 8350 2250 0    50   Input ~ 0
IO77
Text HLabel 8350 2350 0    50   Input ~ 0
IO78
Text HLabel 7450 2050 0    50   Input ~ 0
IO18
Text HLabel 7450 2150 0    50   Input ~ 0
IO19
Text HLabel 7450 2250 0    50   Input ~ 0
IO20
Text HLabel 7450 2350 0    50   Input ~ 0
IO21
Text HLabel 7450 1550 0    50   Input ~ 0
IO6
Text HLabel 7450 1650 0    50   Input ~ 0
IO7
Text HLabel 7450 1750 0    50   Input ~ 0
IO16
Text HLabel 7450 1850 0    50   Input ~ 0
IO17
Text HLabel 7450 1050 0    50   Input ~ 0
IO2
Text HLabel 7450 1150 0    50   Input ~ 0
IO3
Text HLabel 7450 1250 0    50   Input ~ 0
IO4
Text HLabel 7450 1350 0    50   Input ~ 0
IO5
Wire Wire Line
	5850 1750 6050 1750
Text Label 6050 1750 0    50   ~ 0
IO30_ICLK
Text HLabel 5850 1750 0    50   Input ~ 0
IO30_ICLK
Text Label 1550 4450 2    50   ~ 0
IO30_ICLK
Wire Wire Line
	1550 4450 1650 4450
$Comp
L Device:C_Small C3509
U 1 1 5F66265A
P 3750 1200
F 0 "C3509" H 3842 1246 50  0000 L CNN
F 1 "100n" H 3842 1155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3750 1200 50  0001 C CNN
F 3 "~" H 3750 1200 50  0001 C CNN
	1    3750 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3510
U 1 1 5F662660
P 3750 1600
F 0 "C3510" H 3842 1646 50  0000 L CNN
F 1 "100n" H 3842 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3750 1600 50  0001 C CNN
F 3 "~" H 3750 1600 50  0001 C CNN
	1    3750 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1100 3750 1100
Wire Wire Line
	3750 1400 3750 1300
Wire Wire Line
	3750 1500 3750 1400
Connection ~ 3750 1400
Wire Wire Line
	3350 1400 3750 1400
Wire Wire Line
	3350 1700 3750 1700
Wire Wire Line
	3850 6350 3950 6350
Wire Wire Line
	3850 6150 3950 6150
Wire Wire Line
	3850 5950 3950 5950
Wire Wire Line
	3850 5750 3950 5750
Wire Wire Line
	3850 5550 3950 5550
Wire Wire Line
	3850 5350 3950 5350
Wire Wire Line
	3850 5150 3950 5150
Wire Wire Line
	3850 4450 3950 4450
Wire Wire Line
	3850 4750 3950 4750
Wire Wire Line
	3850 4650 3950 4650
Wire Wire Line
	3850 4550 3950 4550
Wire Wire Line
	3850 4350 3950 4350
Wire Wire Line
	3850 4250 3950 4250
Wire Wire Line
	3850 4150 3950 4150
Wire Wire Line
	3850 4050 3950 4050
Wire Wire Line
	3850 3950 3950 3950
Wire Wire Line
	3850 3850 3950 3850
Wire Wire Line
	3850 3750 3950 3750
Wire Wire Line
	3850 3650 3950 3650
Wire Wire Line
	3950 3450 3850 3450
Wire Wire Line
	3950 3350 3850 3350
Wire Wire Line
	3850 3150 3950 3150
Wire Wire Line
	3850 3050 3950 3050
Wire Wire Line
	3850 2950 3950 2950
Wire Wire Line
	3850 2850 3950 2850
Wire Wire Line
	3850 2750 3950 2750
Wire Wire Line
	3850 2650 3950 2650
Wire Wire Line
	3850 2550 3950 2550
Wire Wire Line
	3850 2450 3950 2450
$Comp
L CPLD_Altera:EPM570T100 U3501
U 1 1 5F5D7EDC
P 2750 4450
F 0 "U3501" H 3450 6850 50  0000 C CNN
F 1 "EPM570T100" H 3500 6750 50  0000 C CNN
F 2 "Package_QFP:LQFP-100_14x14mm_P0.5mm" H 3250 2200 50  0001 L CNN
F 3 "https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/hb/max2/max2_mii5v1.pdf" H 2750 4450 50  0001 C CNN
	1    2750 4450
	1    0    0    -1  
$EndComp
Text Label 1550 4850 2    50   ~ 0
IO36
Text Label 1550 4750 2    50   ~ 0
IO35
Wire Wire Line
	1650 5950 1550 5950
Text Label 1550 5950 2    50   ~ 0
IO51
Wire Wire Line
	1650 4650 1550 4650
Text Label 1550 4650 2    50   ~ 0
IO34
Wire Wire Line
	2250 6750 2250 6900
Connection ~ 2250 6900
Wire Wire Line
	2250 6900 2350 6900
Connection ~ 2350 6900
Wire Wire Line
	2350 6900 2450 6900
Wire Wire Line
	2350 6750 2350 6900
Wire Wire Line
	2350 2150 2350 1950
Connection ~ 2350 1950
Wire Wire Line
	2350 1950 2450 1950
Wire Wire Line
	2250 2150 2250 1950
Connection ~ 2250 1950
Wire Wire Line
	2250 1950 2350 1950
Wire Wire Line
	3850 4950 3950 4950
Text Label 3950 4950 0    50   ~ 0
IO82
Text Label 3950 5150 0    50   ~ 0
IO84
$EndSCHEMATC
